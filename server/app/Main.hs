module Main where

import ClassyPrelude

import Control.Monad.Metrics (metricsStore)
import Lens.Micro.Platform   ((^.))


import App.Application       (App (..), AppConfig (..), prepApp, prepConfig)
import App.Hooks             (messageHook)
import Common.Main           (withKillSwitch)
import Common.Run            (AppPrep (..))
import Common.Type.App       (CommonApp (..))
import Poomas.Server         (setEKGStore, setLogger, setMessageHook, startHubRefresher)
import Scribe.Katip          (rioToPoomasLog)


-- | Run application server
main :: IO ()
main = withKillSwitch prepConfig prepApp $ \(AppConfig{}, AppPrep{..}, phcEnv) -> do
  startHubRefresher phcEnv
  setEKGStore       phcEnv . Just $ (capMetrics . appCommon $ apApp) ^. metricsStore
  setLogger         phcEnv . rioToPoomasLog $ apApp
  setMessageHook    phcEnv $ Just (messageHook apApp)
