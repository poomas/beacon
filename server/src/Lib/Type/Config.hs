module Lib.Type.Config
  ( module X
  , AppConfig (..)
  )
where

import Lib.Imports                 hiding (fromList, toLower)

import Data.Aeson                  (FromJSON (..), ToJSON (..), Value (..), genericToJSON,
                                    withObject)
import Lens.Micro.Platform         (lens)


import Common.Type.App             (HasCommonConfigL (..))
import Common.Type.Config          as X
import Scribe.Instance.ConsoleFile ()


-- | Application static configuration read from YAML file or environment vars
data AppConfig = AppConfig
  { cfgCommon :: CommonConfig -- ^ common configuration data
  } deriving (Show, Eq, Generic)
instance ToJSON           AppConfig where toJSON = genericToJSON $ jsonOpts 3
instance HasCommonConfigL AppConfig where
  commonConfigL = lens cfgCommon (\x y -> x{cfgCommon = y})


instance FromJSON (NeedDataAppConfig AppConfig) where
  parseJSON = withObject "AppConfig" $ \o -> do
    ndac  <- unAppConfigData <$> parseJSON (Object o)
    let cfgCommon = error "Impossible happened: FromJSON (NeedDataAppConfig AppConfig)"
    pure $ NeedDataAppConfig $ \d -> AppConfig{..}{ cfgCommon = ndac d}
