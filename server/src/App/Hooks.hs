module App.Hooks where

import App.Imports

import Common.Functions (handleStdMessages)
import Poomas.API       (MessageData (..), MessageLoad (..))
import Poomas.Server    (MessageHook)


messageHook :: App -> MessageHook
messageHook app ml@MessageLoad{..} = runRIO app $
  handleStdMessages ml >>= \case
    Just (MessageCustom t) -> do
      say $ "Custom message from " <> toS (showBaseUrl msgLFrom) <> ":\n"
        <> t
    _ -> pure ()
