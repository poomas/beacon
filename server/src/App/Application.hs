module App.Application
  ( App (..)
  , AppConfig (..)

  , prepConfig
  , prepApp
  )
where

import App.Imports

import Control.Monad.Except   (ExceptT (..))
import Network.Wai            (Middleware)
import Servant.API.Generic    (ToServant, ToServantApi)
import Servant.Server.Generic (AsServerT, genericServerT)

import Beacon.API             (BeaconRoutes (..))
import Common.API.Routes      (commonAdminApi)
import Common.Run             (AppPrep (..), prepCommonApp)
import Common.StartupLog      (startupLog)
import Common.Type.App        (mkCommonApp)
import Poomas.Server          (PhcEnv)
import Scribe.Katip           (DynPermMap, logInfoK)


-- | Record of 'Handler's for the whole API
beaconApi :: ToServant BeaconRoutes (AsServerT AppM)
beaconApi = genericServerT BeaconRoutes
  { beaconAdminR = commonAdminApi
  , beaconMiscR  = miscFunction
  }

miscFunction :: Int -> AppM Text
miscFunction errCode = do
  putStrLn "IN MISC"
  throwM err300 {errHTTPCode = errCode, errBody="some error"}


-- | Proxy type for the whole API represented by this app
fullApi :: Proxy (ToServantApi BeaconRoutes)
fullApi = Proxy


prepConfig :: FilePath -> IO AppConfig
prepConfig cfgFile = do
  isDev <- ("production" `notElem`) <$> getArgs
  parseCommonConfig isDev [cfgFile] []

-- | Start the application server. It is expected to run the server as a forked
--   process, and `shutdownSwitch` enables "almost" clean shutting it down. It is
--   an empty MVar, and when it gets filled, the main thread, which is waiting on
--   this MVar, will exit, closing forked server as well.
--   Server is run in `try` block, so any exception should result in `Left` exit.
prepApp :: AppConfig -> PhcEnv -> IO (AppPrep App)
prepApp cfg phcEnv = do
  r@AppPrep{..} <- prepCommonApp cfg phcEnv mkApplication
  runKatipContextT apKatipLE () "init"
    . logInfoK . fromString . toS . logConfig $ appConfig apApp
  pure r


-- | Create `App` record (holding application state in `Reader`), WAI application
--   and `Middleware`s. If `App` state creation was successful, initialises DB
--   (i.e. runs migration, inserts initial data, etc)
--   Return created `App`, WAI application and mappended list of middlewares.
mkApplication :: AppConfig -> LogEnv -> IORef DynPermMap -> PhcEnv
              -> KatipContextT IO (App, Application, Middleware)
mkApplication cfg katipLE dpm phcEnv = do
  let ctx = EmptyContext
  try (mkApp cfg katipLE dpm phcEnv) >>= \case
    Left  (e :: SomeException) -> throwM e
    Right (app, middlewares)   -> do
      let waiApp  = mkAppM fullApi ctx app $ beaconApi
      pure (app, waiApp, foldl1 (.) middlewares)

  where
    mkAppM :: forall api a.
              ( HasServer api a
              , HasContextEntry (a .++ DefaultErrorFormatters) ErrorFormatters
              )
           => Proxy api -> Context a -> App -> ServerT api AppM -> Application
    mkAppM api sctx appCfg =
      serveWithContext api sctx . hoistServerWithContext api (Proxy @a) convertApp
      where
        convertApp :: AppM v -> Handler v
        convertApp app = Handler . ExceptT . try $ runReaderT (unRIO app) appCfg


-- | Prepare `App` state from external configuration (file and ENV). If configured,
--   forks EKG server, prepares metrics and logging.
--   Prepares a list of possible WAI middlewares:
--     - metrics
--     - Basic HTTP Auth for the whole site with 1 credential
--     - add predefined static `Headers` to each response
--     - WAI request logger
--     - specified `Request` timeout
--     - gzip payload
--   Return created `App` and list of `Middleware's.
mkApp :: AppConfig
      -> LogEnv
      -> IORef DynPermMap
      -> PhcEnv
      -> KatipContextT IO (App, [Middleware])
mkApp cfg@AppConfig{..} katipLE dpm phcEnv = do
  -- let CommonConfig{..} = cfgCommon
  (cmnApp, mWares) <- mkCommonApp cfgCommon katipLE dpm phcEnv fullApi
  let app = App { appCommon = cmnApp
                , appConfig = cfg
                }
  pure ( app, mWares)



-- | Create log line with current configuration from YAML and/or ENV
logConfig :: AppConfig -> Text
logConfig = startupLog . cfgCommon
