{-# OPTIONS_GHC -fno-warn-orphans #-}
module Orphans where

import Prelude

import Data.String.Conv               (toS)
import RIO.Text                       (Text)
import Test.QuickCheck
import Test.QuickCheck.Instances.Time ()


-- | Generate non-empty Text values
instance Arbitrary Text where
    arbitrary = toS @String <$> arbitraryNoEmpty
    shrink xs = toS <$> shrink xs

-- | Generate random valid port
arbitraryPort :: Gen Int
arbitraryPort = choose (80,65535)

arbitraryNoEmpty :: Arbitrary a => Gen [a]
arbitraryNoEmpty = getNonEmpty <$> arbitrary

letters :: String
letters = ['a' .. 'z'] <> ['A' .. 'Z']

properName :: Gen Text
properName = toS <$> (listOf1 . elements $ letters <> ['0'..'9'] <> ".-_")
