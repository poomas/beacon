{
    "swagger": "2.0",
    "info": {
        "version": "0.1.0",
        "title": "Beacon Swagger API",
        "license": {
            "url": "https://opensource.org/licenses/BSD-3-Clause",
            "name": "BSD3"
        },
        "description": "API specification for poomas Beacon service"
    },
    "definitions": {
        "OfferedService": {
            "required": [
                "service",
                "path"
            ],
            "type": "object",
            "description": "An offered service by hub",
            "properties": {
                "path": {
                    "type": "string"
                },
                "service": {
                    "$ref": "#/definitions/Service"
                }
            }
        },
        "Hub": {
            "required": [
                "uri",
                "need",
                "offer",
                "dataState"
            ],
            "type": "object",
            "description": "Hub definition",
            "properties": {
                "dataState": {
                    "$ref": "#/definitions/HubDataState"
                },
                "offer": {
                    "$ref": "#/definitions/Offer"
                },
                "uri": {
                    "$ref": "#/definitions/URI"
                },
                "need": {
                    "$ref": "#/definitions/Need"
                }
            }
        },
        "LoadBalancerType": {
            "example": "round-robin or always-first",
            "description": "Type of tween balancer for these services"
        },
        "Service": {
            "required": [
                "name",
                "version",
                "lBType"
            ],
            "type": "object",
            "description": "A service offered or needed by hub",
            "properties": {
                "name": {
                    "type": "string"
                },
                "version": {
                    "$ref": "#/definitions/Version"
                },
                "lBType": {
                    "$ref": "#/definitions/LoadBalancerType"
                }
            }
        },
        "Offer": {
            "example": {
                "service": [
                    {
                        "path": "http://localhost:3003",
                        "service": {
                            "name": "service1",
                            "version": "v1.2.3",
                            "lBType": "round-robin"
                        }
                    }
                ],
                "gateway": [],
                "custom": [
                    {
                        "alive": true
                    }
                ],
                "monitor": [
                    {
                        "event": [
                            "swarm"
                        ],
                        "name": "MyName"
                    }
                ],
                "info": [
                    [
                        "Question",
                        "The answer"
                    ]
                ]
            },
            "required": [
                "service",
                "info",
                "monitor",
                "gateway",
                "custom"
            ],
            "type": "object",
            "description": "Hub offers",
            "properties": {
                "service": {
                    "uniqueItems": true,
                    "items": {
                        "$ref": "#/definitions/OfferedService"
                    },
                    "type": "array"
                },
                "gateway": {
                    "uniqueItems": true,
                    "items": {
                        "$ref": "#/definitions/Offer"
                    },
                    "type": "array"
                },
                "custom": {
                    "uniqueItems": true,
                    "items": {
                        "$ref": "#/definitions/Value"
                    },
                    "type": "array"
                },
                "monitor": {
                    "uniqueItems": true,
                    "items": {
                        "$ref": "#/definitions/Monitor"
                    },
                    "type": "array"
                },
                "info": {
                    "uniqueItems": true,
                    "items": {
                        "minItems": 2,
                        "items": [
                            {
                                "type": "string"
                            },
                            {
                                "type": "string"
                            }
                        ],
                        "maxItems": 2,
                        "type": "array"
                    },
                    "type": "array"
                }
            }
        },
        "Severity": {
            "example": "Debug",
            "type": "string",
            "enum": [
                "Debug",
                "Info",
                "Warning",
                "Error",
                "Notice",
                "Critical",
                "Alert",
                "Emergency"
            ],
            "description": "Severity defines which log messages to log"
        },
        "Value": {
            "minProperties": 1,
            "maxProperties": 1,
            "type": "object",
            "properties": {
                "String": {
                    "type": "string"
                },
                "Null": {
                    "example": [],
                    "items": {},
                    "maxItems": 0,
                    "type": "array"
                },
                "Array": {
                    "items": {
                        "$ref": "#/definitions/Value"
                    },
                    "type": "array"
                },
                "Object": {
                    "$ref": "#/definitions/Object"
                },
                "Number": {
                    "type": "number"
                },
                "Bool": {
                    "type": "boolean"
                }
            }
        },
        "URI": {
            "example": "localhost:3004",
            "type": "string",
            "description": "Base URI of hub"
        },
        "Monitor": {
            "required": [
                "name"
            ],
            "type": "object",
            "description": "A monitor offered or needed by hub",
            "properties": {
                "event": {
                    "items": {
                        "$ref": "#/definitions/EventType"
                    },
                    "type": "array"
                },
                "uri": {
                    "items": {
                        "type": "string"
                    },
                    "type": "array"
                },
                "name": {
                    "type": "string"
                }
            }
        },
        "EventType": {
            "type": "string",
            "enum": [
                "swarm",
                "track",
                "log",
                "exception",
                "other"
            ],
            "description": "Event types Monitor accepts"
        },
        "Need": {
            "example": {
                "service": [
                    {
                        "name": "service1",
                        "version": "v1.2.3",
                        "lBType": "round-robin"
                    }
                ],
                "gateway": [],
                "monitor": [
                    {
                        "event": [
                            "swarm"
                        ],
                        "name": "MyName"
                    }
                ],
                "info": [
                    "Question"
                ]
            },
            "required": [
                "service",
                "info",
                "monitor",
                "gateway"
            ],
            "type": "object",
            "description": "Hub needs",
            "properties": {
                "service": {
                    "uniqueItems": true,
                    "items": {
                        "$ref": "#/definitions/Service"
                    },
                    "type": "array"
                },
                "gateway": {
                    "uniqueItems": true,
                    "items": {
                        "$ref": "#/definitions/Need"
                    },
                    "type": "array"
                },
                "monitor": {
                    "uniqueItems": true,
                    "items": {
                        "$ref": "#/definitions/Monitor"
                    },
                    "type": "array"
                },
                "info": {
                    "uniqueItems": true,
                    "items": {
                        "type": "string"
                    },
                    "type": "array"
                }
            }
        },
        "Version": {
            "example": "v1.2.3.4",
            "type": "string",
            "description": "Service version string"
        },
        "Object": {
            "additionalProperties": true,
            "type": "object",
            "description": "Arbitrary JSON object."
        },
        "HubDataState": {
            "minProperties": 1,
            "maxProperties": 1,
            "type": "object",
            "description": "Hub data state types",
            "properties": {
                "old": {
                    "example": [],
                    "items": {},
                    "maxItems": 0,
                    "type": "array"
                },
                "current": {
                    "example": [],
                    "items": {},
                    "maxItems": 0,
                    "type": "array"
                },
                "deleted": {
                    "maximum": 255,
                    "minimum": 0,
                    "type": "integer"
                }
            }
        }
    },
    "paths": {
        "/admin/alive": {
            "get": {
                "summary": "Return true is server is alive.",
                "responses": {
                    "200": {
                        "schema": {
                            "type": "boolean"
                        },
                        "description": ""
                    }
                },
                "produces": [
                    "application/json;charset=utf-8"
                ],
                "operationId": "getAdminAlive"
            }
        },
        "/admin/debug/{scribe}/{level}": {
            "post": {
                "summary": "Changes dynamically Severity for the app",
                "responses": {
                    "404": {
                        "description": "`scribe` or `level` not found"
                    },
                    "200": {
                        "schema": {
                            "type": "string"
                        },
                        "description": ""
                    }
                },
                "produces": [
                    "application/json;charset=utf-8"
                ],
                "parameters": [
                    {
                        "required": true,
                        "in": "path",
                        "name": "scribe",
                        "type": "string",
                        "description": "Name of the scribe to modify or 'all' for all scribes"
                    },
                    {
                        "required": true,
                        "in": "path",
                        "name": "level",
                        "type": "string",
                        "enum": [
                            "Debug",
                            "Info",
                            "Warning",
                            "Error",
                            "Notice",
                            "Critical",
                            "Alert",
                            "Emergency"
                        ],
                        "description": "New Severity to be set"
                    }
                ],
                "operationId": "postAdminDebug"
            }
        },
        "/misc/{code}": {
            "get": {
                "summary": "Just a tester.",
                "responses": {
                    "404": {
                        "description": "`code` not found"
                    },
                    "200": {
                        "schema": {
                            "type": "string"
                        },
                        "description": ""
                    }
                },
                "produces": [
                    "application/json;charset=utf-8"
                ],
                "parameters": [
                    {
                        "maximum": 9223372036854775807,
                        "minimum": -9223372036854775808,
                        "required": true,
                        "in": "path",
                        "name": "code",
                        "type": "integer"
                    }
                ],
                "operationId": "getMisc"
            }
        },
        "/admin/shutdown": {
            "get": {
                "summary": "Shutdowns the server",
                "responses": {
                    "200": {
                        "schema": {
                            "type": "string"
                        },
                        "description": ""
                    }
                },
                "produces": [
                    "application/json;charset=utf-8"
                ],
                "operationId": "getAdminShutdown"
            }
        },
        "/admin/swarm/state": {
            "get": {
                "summary": "Return the current state of the swarm.",
                "responses": {
                    "200": {
                        "schema": {
                            "minItems": 2,
                            "items": [
                                {
                                    "$ref": "#/definitions/Hub"
                                },
                                {
                                    "items": {
                                        "$ref": "#/definitions/Hub"
                                    },
                                    "type": "array"
                                }
                            ],
                            "maxItems": 2,
                            "type": "array"
                        },
                        "description": ""
                    }
                },
                "produces": [
                    "application/json;charset=utf-8"
                ],
                "operationId": "getAdminSwarmState"
            }
        },
        "/admin/debug": {
            "get": {
                "responses": {
                    "200": {
                        "schema": {
                            "items": {
                                "minItems": 2,
                                "items": [
                                    {
                                        "type": "string"
                                    },
                                    {
                                        "$ref": "#/definitions/Severity"
                                    }
                                ],
                                "maxItems": 2,
                                "type": "array"
                            },
                            "type": "array"
                        },
                        "description": ""
                    }
                },
                "produces": [
                    "application/json;charset=utf-8"
                ],
                "operationId": "getAdminDebug"
            }
        }
    }
}