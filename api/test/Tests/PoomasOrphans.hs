{-# OPTIONS_GHC -fno-warn-orphans #-}
module Tests.PoomasOrphans where

import           Prelude

import           Data.Aeson                     (FromJSON (..))
import           Data.String.Conv               (toS)
import           Data.Swagger                   (Swagger)
import           Katip                          (Severity (..))
import           Lens.Micro.Platform            ((%~), (&))
import qualified RIO.Text                       as T
import           Servant.API                    (NoContent (..))
import           Servant.Client                 (BaseUrl (..))
import           Servant.Client.Core            (Scheme (..))
import           Test.QuickCheck
import           Test.QuickCheck.Instances.Time ()
import           Test.QuickCheck.Instances.UUID ()


import           Poomas.API


-- | Generate non-empty Text values
instance Arbitrary T.Text where
    arbitrary = T.pack <$> arbitraryNoEmpty
    shrink xs = T.pack <$> shrink (T.unpack xs)

-- | Generate random valid port
arbitraryPort :: Gen Int
arbitraryPort = choose (80,65535)

arbitraryNoEmpty :: Arbitrary a => Gen [a]
arbitraryNoEmpty = getNonEmpty <$> arbitrary

letters :: String
letters = ['a' .. 'z'] <> ['A' .. 'Z']

properName :: Gen T.Text
properName = toS <$> (listOf1 . elements $ letters <> ['0'..'9'] <> ".-_")

instance Arbitrary Scheme where arbitrary = oneof [pure Http, pure Https]

instance (Arbitrary a, CachedValue a) => Arbitrary (Cached a) where
  arbitrary = toCached <$> arbitrary

instance Arbitrary BaseUrl where
  arbitrary = BaseUrl <$> arbitrary     -- baseUrlScheme
                      <*> hostNameGen   -- baseUrlHost
                      <*> arbitraryPort -- baseUrlPort
                      <*> pathGen       -- baseUrlPath
    where
      pathGen     =  ("/" <>) <$> listOf1 (elements letters)
      hostNameGen = do
        start  <- elements letters
        middle <- listOf1 $ elements (letters <> ['0' .. '9'] <> ['.', '-'])
        end    <- elements letters
        return (start : middle <> [end])

instance Arbitrary PoomasRefresh where
  arbitrary = PoomasRefresh <$> arbitrary -- srRetryDelaySec
                            <*> arbitrary -- srMaxLevels
                            <*> arbitrary -- srHeartbeat

instance Arbitrary HubDataState where
  arbitrary = oneof [pure HdsOld, pure HdsCurrent, HdsDeleted <$> arbitrary]

instance Arbitrary Hub where
  arbitrary = Hub <$> arbitrary -- hubUri
                  <*> arbitrary -- hubOffer
                  <*> arbitrary -- hubNeed
                  <*> arbitrary -- hubDataState

instance Arbitrary LoadBalancerType where
  arbitrary = oneof [pure LbtRoundRobin, pure LbtAlwaysFirst]


instance Arbitrary Service where
  arbitrary = Service <$> properName -- serviceName
                      <*> arbitrary  -- serviceVersion
                      <*> arbitrary  -- serviceTBalancer

instance Arbitrary OfferedService where
  arbitrary = OfferedService <$> arbitrary  -- offerSrvService
                             <*> arbitrary  -- offerSrvUri

instance Arbitrary Version where
  arbitrary = Version <$> properName -- unVersion

instance Arbitrary Need where
  arbitrary = Need <$> arbitrary    -- needService
                   <*> arbitrary    -- needInfo
                   <*> arbitrary    -- needMonitor
                   <*> pure mempty  -- needGateway

instance Arbitrary Offer where
  arbitrary = Offer <$> arbitrary   -- offerService
                    <*> arbitrary   -- offerInfo
                    <*> arbitrary   -- offerMonitor
                    <*> pure mempty -- offerGateway
                    <*> pure mempty -- offerCustom

-- Monitor part

instance Arbitrary Monitor where
  arbitrary = Monitor <$> properName -- monitorName
                      <*> arbitrary  -- monitorEvent

instance Arbitrary MonitorLoad where
  arbitrary = MonitorLoad <$> arbitrary -- mlFrom
                          <*> arbitrary -- mlRecvOrder
                          <*> arbitrary -- mlData

instance Arbitrary EventType where
  arbitrary = oneof [ pure EtSwarm
                    , pure EtTrack
                    , pure EtLog
                    , pure EtException
                    , pure EtOther
                    ]

instance Arbitrary PoomasEventLoad where
  arbitrary = PoomasEventLoad <$> arbitrary -- pelHubs
                              <*> arbitrary -- pelMessage

instance Arbitrary PoomasLogLoad where
  arbitrary = PoomasLogLoad <$> arbitrary -- pllOrigin
                            <*> arbitrary -- pllMessage

instance Arbitrary PoomasExceptionLoad where
  arbitrary = PoomasExceptionLoad <$> arbitrary -- pexlOrigin
                                  <*> arbitrary -- pexlMessage

instance Arbitrary PoomasTrackLoad where
  arbitrary = PoomasTrackLoad <$> arbitrary -- ptlUuid
                              <*> arbitrary -- ptlMessage

instance Arbitrary EventData where
  arbitrary = oneof [ EventPoomas     <$> arbitrary
                    , EventTrack      <$> arbitrary
                    , EventLog        <$> arbitrary
                    , EventException  <$> arbitrary
                    ]


  -- Message part

instance Arbitrary Message where
  arbitrary = Message <$> properName -- messageName
                      <*> arbitrary  -- messageEvent


instance Arbitrary MessageType where
  arbitrary = oneof [ pure MtLog
                    , pure MtCustom
                    ]
instance Arbitrary MessageData where
  arbitrary = oneof [ MessageLog    <$> arbitrary
                    , MessageCustom <$> arbitrary
                    ]

instance Arbitrary MessageLoad where
  arbitrary = MessageLoad <$> arbitrary -- msgLFrom
                          <*> arbitrary -- msgLRcptOrder
                          <*> arbitrary -- msgLData

instance Arbitrary LogFilterLoad where
  arbitrary = LogFilterLoad <$> arbitrary -- lflReset
                            <*> arbitrary -- lflSeverity
                            <*> arbitrary -- lflPrefixes
                            <*> arbitrary -- lflNamespaces
                            <*> arbitrary -- lflPeriod
                            <*> arbitrary -- lflHost
                            <*> arbitrary -- lflModule



instance Arbitrary PoomasConfig where
  arbitrary = PoomasConfig <$> arbitrary  -- pCfgCfgBeacons
                           <*> arbitrary  -- pCfgRefresh
                           <*> bUrlNoPath -- pCfgLocal
    where
      bUrlNoPath = (& hubUri %~ (\b -> b{baseUrlPath =""})) <$> arbitrary

instance FromJSON PoomasConfig where
  parseJSON x = do
    NeedData n <- parseJSON x
    pure $ n Nothing Nothing PortNever


instance Arbitrary NoContent where arbitrary = pure NoContent

instance Arbitrary Swagger   where arbitrary = arbitrary

instance Arbitrary PortSetter where
  arbitrary = oneof [ PortAlways    <$> arbitrary
                    , PortIfMissing <$> arbitrary
                    , pure PortNever
                    ]

instance Arbitrary Severity where
  arbitrary = oneof [ pure DebugS
                    , pure InfoS
                    , pure NoticeS
                    , pure WarningS
                    , pure ErrorS
                    , pure CriticalS
                    , pure AlertS
                    , pure EmergencyS
                    ]
