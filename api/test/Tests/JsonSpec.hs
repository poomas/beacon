module Tests.JsonSpec (spec) where

import RIO

import Servant.Swagger       (validateEveryToJSON)
import Test.Hspec            (Spec, describe)
import Test.Hspec.QuickCheck (modifyMaxSize, modifyMaxSuccess)

-- import Data.Aeson            (FromJSON, ToJSON, encode, parseJSON, toJSON)
-- import Data.Aeson.Types      (parseEither)
-- import Data.String.Conv      (toS)
-- import Data.Typeable         (typeOf)
-- import Test.Hspec.QuickCheck (prop)
-- import Test.QuickCheck       hiding (Result, Success)

import Beacon.API
import Tests.PoomasOrphans   () -- should be exportes by hub-api or separate package


spec :: Spec
spec = do
  describe "All routes: ToJSON matches ToSchema"
    . modifyMaxSize (const 30)
    . modifyMaxSuccess (const 20)
    $ validateEveryToJSON beaconRoutesApi



-- propJSON :: forall a . (Arbitrary a, ToJSON a, FromJSON a, Show a, Eq a, Typeable a)
--          => Proxy a -> Spec
-- propJSON _ = prop testName $ \(a :: a) ->
--   let json = "with " <> toS (encode a)
--    in counterexample json (parseEither parseJSON (toJSON a) === Right a)
--   where
--     testName = show ty <> " FromJSON/ToJSON checks"
--     ty       = typeOf (undefined :: a)
