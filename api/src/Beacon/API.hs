module Beacon.API
  ( module X

  , BeaconRoutes (..)
  , beaconRoutesApi
  , generateSwagger

  )
where

import Beacon.Imports      hiding (serveDirectoryWebApp)

import Data.Swagger        ()
import Servant.API.Generic ((:-), ToServantApi, genericApi)


import Beacon.Type.Orphans as X ()
import Common.API.Admin    as X (CommonAdminRoutes (..))
import Common.API.Swagger  (commonGenSwagger)


-- | Fetch a tracked session
type MiscRoute =
     Summary "Just a tester."
  :> Capture "code" Int
  :> Get '[JSON] Text

------------------------------------------------------------------------------------------
-- Routes/API
------------------------------------------------------------------------------------------

data BeaconRoutes r = BeaconRoutes
  { beaconAdminR :: r :- "admin" :> ToServantApi CommonAdminRoutes
  , beaconMiscR  :: r :- "misc"  :>              MiscRoute
  } deriving (Generic)


beaconRoutesApi :: Proxy (ToServantApi BeaconRoutes)
beaconRoutesApi = genericApi (Proxy :: Proxy BeaconRoutes)


-- | Output generated @swagger.json@ file.
generateSwagger :: FilePath -> IO ()
generateSwagger = commonGenSwagger
  "Beacon Swagger API"
  "API specification for poomas Beacon service"
  (genericApi (Proxy :: Proxy BeaconRoutes))
