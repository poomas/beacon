module Main where

import Prelude

import Beacon.API (generateSwagger)


-- | Run application server
main :: IO ()
main = do
  let outputDir = "docs"
  generateSwagger $ outputDir <> "/swagger.json"
  print $ "'swagger.json' file placed in " <> outputDir
